# poppy-emoji-proposal
A proposal to the Unicode Consortium regarding the inclusion of a poppy emoji. The submission has been sent to Unicode and is awaiting review (probably sometime in 2019), but feel free to discuss anything in the Issues section of this GitHub repository.

<img src="https://github.com/inferno986return/poppy-emoji-proposal/blob/master/img/poppy.png" alt="Poppy proof-of-concept"/>

## Background

The Remembrance Poppy is a well known symbol here in the UK and in other Commonwealth countries. Every year on the 11th November at 11:00am, many take a silence in memory of those lost in conflicts both in the past and in the present day.

The poppy is everywhere, so naturally as an important symbol I was surprised to realise that the *Papaver rhoeas* flower is not represented in the Unicode standard. Then as I googled around, I noticed I wasn't the only one would wanted to use it.

So I started a proposal and when I reached a point when I thought it was ready to submit I sent it. However, in case of rejection (where I may need some ideas from the community) and for archival purposes I have uploaded the proposal to GitHub.

## Licensing

<img src="https://github.com/inferno986return/poppy-emoji-proposal/blob/master/img/ccbynd.png" alt="CC-BY-ND"/> 

The proposal itself (as a .pdf, .tex, .docx etc.) is licensed under the [Creative Commons Attribution-NoDerivatives 4.0 International (CC BY-ND 4.0)](https://creativecommons.org/licenses/by-nd/4.0/). I don't usually use no derivatives but I would like to maintain one definitive version of this document.

The poppy graphic itself was taken from OpenClipArt and is available in the public domain. This is a proof-of-concept design and not the final version.

Other images in the proposal are either fair use or under a sharealike license. I have linked to them in the proposal.